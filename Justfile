IMAGE := env_var_or_default("IMAGE", "<name>")
VERSION := env_var_or_default("VERSION", "latest")


default:
  @echo "IMAGE: {{IMAGE}} VERSION: {{VERSION}}"
  @just --list

tailwind *args:
  yarn tailwindcss -i ./static/css/input.scss -o ./static/css/output.css --minify {{args}}

check-uncommitted:
  @git diff --stat HEAD --exit-code -- . || (echo "Please commit pending changes before releasing" && exit -1)

init-data:
  docker-compose run web ./manage.py migrate
  docker-compose run web ./manage.py createsuperuser --username django --email carles@barrobes.com

clean-data:
  docker-compose down --volumes

lint:
  black --exclude collected --exclude node_modules .

build:
  docker-compose build web

test:
  docker-compose run web pytest -sv --cov --cov-report term --cov-report html:_reports/coverage-html

infra:
  docker-compose up db

run-nodebug: stop
  DJANGO_DEBUG=False docker-compose up --build

run: stop
  docker-compose up --build

stop:
  docker-compose down

package:
  docker build -t {{IMAGE}}:{{VERSION}} -t txels/{{IMAGE}}:{{VERSION}} .

publish:
  docker push txels/{{IMAGE}}:{{VERSION}}

collectstatic *args:
  docker-compose run web ./manage.py collectstatic --noinput {{args}}

deploystatic:
  s3cmd sync --acl-public collected/* s3://{{IMAGE}}/static/
  just remime

remime:
  # Ensure CSS/SVG files get the right mime type
  s3cmd --recursive modify --mime-type image/svg+xml --add-header="Cache-Control:max-age=3600" s3://{{IMAGE}}/static/img/*.svg
  s3cmd --recursive modify --mime-type text/css --add-header="Cache-Control:max-age=3600" s3://{{IMAGE}}/static/css/
  s3cmd --recursive modify --mime-type text/css --add-header="Cache-Control:max-age=3600" s3://{{IMAGE}}/static/admin/css/
  s3cmd --recursive modify --mime-type text/css --add-header="Cache-Control:max-age=3600" s3://{{IMAGE}}/static/debug_toolbar/css/
  s3cmd --recursive modify --mime-type text/css --add-header="Cache-Control:max-age=3600" s3://{{IMAGE}}/static/rest_framework/css/

distill:
  STATICSERVE=whitenoise ./manage.py collectstatic --noinput
  STATICSERVE=whitenoise ./manage.py distill-local --quiet --force distilled

browse-distilled:
  python -m http.server --directory distilled 8111

alias deploy := deploy-distilled

deploy-distilled: distill
  s3cmd put --recursive --acl-public distilled/static/css/*.css s3://{{IMAGE}}/static/css/ --mime-type text/css --add-header="Cache-Control:max-age=360000"
  s3cmd sync --acl-public distilled/* s3://{{IMAGE}}/

k8s-context:
  kubectl config use-context linode

k8s-restart: k8s-context
  kubectl rollout restart deploy/{{IMAGE}}

k8s-deploy: k8s-context
  kubectl apply -f k8s
  kubectl annotate deployment/{{IMAGE}} kubernetes.io/change-cause="$(git show --no-patch --oneline)"

secret-key:
  @poetry run python scripts/make_secret_key.py

k8s-secret-key: k8s-context
  kubectl delete secret {{IMAGE}}-secrets || echo "Secret missing, skip deletion"
  kubectl create secret generic {{IMAGE}}-secrets --from-literal=secretkey="$(just secret-key)"

release: package publish k8s-restart watch-pods

full-release: check-uncommitted collectstatic test deploystatic package publish k8s-restart watch-pods

watch-pods:
  kubectl get pods -w

openapi-schema:
  docker-compose run web ./manage.py generateschema

# image manipulation recipes

concatenate result *sources:
    convert -append {{sources}} {{result}}

scale source percent:
    convert -scale {{percent}}% {{source}} scaled-{{source}}

split-vertical source target vpercent:
    convert -crop "100%x{{vpercent}}%" "{{source}}" "{{target}}.jpg"
