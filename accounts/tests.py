# accounts/tests.py
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse, resolve
from django.utils.translation import gettext_lazy as _

from allauth.account.models import EmailAddress

User = get_user_model()


class CustomUserTests(TestCase):
    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(
            username="will", email="will@email.com", password="testpass123"
        )
        self.assertEqual(user.username, "will")
        self.assertEqual(user.email, "will@email.com")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        admin_user = User.objects.create_superuser(
            username="superadmin", email="superadmin@email.com", password="testpass123"
        )
        self.assertEqual(admin_user.username, "superadmin")
        self.assertEqual(admin_user.email, "superadmin@email.com")
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)


class SignupPageTests(TestCase):
    username = "newuser"
    email = "newuser@email.com"

    def setUp(self):
        self.url = reverse("account_signup")

    def test_signup_template(self):
        self.response = self.client.get(self.url)
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, "account/signup.html")
        self.assertContains(self.response, _("Sign Up"))

    def test_signup_ok(self):
        EMAIL = "signup_ok@barrobes.com"
        self.response = self.client.post(
            self.url,
            data={
                "email": EMAIL,
                "password1": "fjshgr7y34",
                "password2": "fjshgr7y34",
                "first_name": "Carles",
            },
            follow=True,
        )
        self.assertEqual(self.response.status_code, 200)
        user = User.objects.get(email=EMAIL)
        self.assertFalse(user.is_staff)
        email = EmailAddress.objects.get(email=EMAIL)
        self.assertFalse(email.verified)
