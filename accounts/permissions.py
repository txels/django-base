from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user and (obj.user_id == request.user.id)


class IsOwnerOrReadOnly(IsOwner):
    def has_object_permission(self, request, view, obj):
        # Read-only permissions are allowed for any request:
        if request.method in permissions.SAFE_METHODS:
            return True
        return super().has_object_permission(request, view, obj)
