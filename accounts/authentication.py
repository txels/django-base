import requests
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions


class DelegatedJWTAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth = request.META.get("HTTP_AUTHORIZATION")
        response = requests.get(
            f"{settings.FEATURE_AUTH_SERVER}/api/auth/user/",
            headers={"Authorization": auth},
        )
        data = response.json()
        try:
            user = User(**data)
        except Exception as e:
            raise exceptions.AuthenticationFailed(data)
        return (user, None)
