# Static assets with tailwind

Input: `static/css/input.scss`
Output: `static/css/output.css`

# Compare options of serving static files with distill

## django-staticfiles only

Must set DEBUG=True for it to work

No "manifest". Served file = output.css

`distill-local` generates "hashed" CSS files (unless using `--exclude-staticfiles`) but serves the non-hashed version of `output.css`

## whitenoise

Must include staticfiles app.
Middleware in a very specific location.

DEBUG=True:

- django-staticfiles takes over
- serves non-hashed `output.css`

DEBUG=False

- Must run `collectstatic` manually (else HTTP 500 manifest not found)
- Serves hashed e.g. `output.bc7c78cdd3b5.css`
- A bunch of nice caching headers we won't really use.

`distill-local` generates "hashed" CSS files and serves the non-hashed version of `output.css` (as generated in `index.html`)

> WARN: `distill-local` includes static _and_ media files. It doesn't include only static files, unless MEDIA_ROOT is undefined.

## django-storages "manifest"

- `collectstatic` must be called explicitly, it will upload files to "S3" (takes forever - 1.5min)
- Slightly faster alternative: collect static with storages disabled remove the `admin` static files, and run `s3cmd sync` (35s)

`distill-local`: no need for static files, as they are already deployed to "S3".

# Caveats

- Coupling between "DEBUG" and serving static files in local development.
- `staticfiles` app must always be there to be able to `collectstatic`
- Media Type on upload (e.g. CSS files won't load in browser if media type is not text/css)
- Different caching strategies on HTML and CSS
- No way to disable static collection for admin/rest framework/other non-static parts of the app
