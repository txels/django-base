import os
from uuid import uuid4

from storages.backends.s3boto3 import S3Boto3Storage, S3ManifestStaticStorage


DAYS = 3600 * 24


class MediaStorage(S3Boto3Storage):
    location = "media"
    file_overwrite = False
    object_parameters = {
        "CacheControl": f"max-age={ 30 * DAYS }",
    }


class StaticStorage(S3ManifestStaticStorage):
    location = "static"
    object_parameters = {
        "CacheControl": f"max-age={ 7 * DAYS }",
    }


def normalize_file_name(instance, filename):
    ext = filename.split(".")[-1]
    # set filename as random string
    filename = "{}.{}".format(uuid4().hex, ext)
    return os.path.join(filename)
