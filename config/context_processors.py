def base_template(request):
    is_hx_request = "HX-Request" in request.headers or request.GET.get("x") is not None
    return {
        "BASE_TEMPLATE": "_base_fragment.html" if is_hx_request else "_base.html",
        "is_hx_request": is_hx_request,
    }
