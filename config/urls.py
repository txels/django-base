from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
import debug_toolbar

urlpatterns = [
    # Admin
    path("__adm__/", admin.site.urls),
    # 3rd party apps
    path("accounts/", include("allauth.urls")),
    path("api/", include("api.urls")),
    # language change
    path("i18n/", include("django.conf.urls.i18n")),
    # Local apps
    path("", include("pages.urls")),
]

# media files in local development
if settings.ENVIRONMENT == "dev":
    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]
    if settings.MEDIA_ROOT:
        urlpatterns += [
            # media files
            *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
        ]
