��    '      T  5   �      `  +   a  
   �     �     �  !   �     �     �     �     �  9   
     D     U     ]     n     q     y     �     �     �     �     �     �  B   �          
          &     8     @  @   H  
   �     �     �     �     �     �     �  .   �  U  
  !   `     �     �     �      �     �     �     �     �  .        :     J     R     n     s     x     ~  
   �     �     �     �     �  M   �  	   	     	     +	     3	  
   P	  
   [	  =   f	     �	  
   �	     �	     �	     �	     �	     �	  ,   �	                                           !                              %             &                $         '          
                    "      	         #                     
      Page %(current)s of %(total)s
       Add E-mail Add E-mail Address Admin Are you sure you want to log out? Catalan Change Password Confirm Confirm E-mail Address Do you really want to remove the selected e-mail address? E-mail Addresses English Forgot Password? Hi Log Out Log in Log out Logging Out... Logout Make Primary Note Password Reset Please contact us if you have any trouble resetting your password. Primary Re-send Verification Remove Reset My Password Sign Up Sign up The following e-mail addresses are associated with your account: Unverified Verified Warning: You are an admin You are not logged in next previous you are already logged in as %(user_display)s. Project-Id-Version: babystats 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-10-02 00:07+0100
Last-Translator: Carles Barrobés i Meix <carles@barrobes.com>
Language-Team: Catalan <ca@li.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Pàgina %(current)s de %(total)s Afegeix mail Afegeix adreça de mail Admin N'estàs segura que vols sortir? Català Canvia contrasenya Confirma Confirma adreça de mail De debò vols eliminar l'adreça seleccionada? Adreces de mail Anglès Has oblidat la contrasenya? Hola Surt Entra Surt Sortint... Surt Fes-la primària Nota Reinicia contrasenya Posa't en contacte amb nosaltres si tens problemes reiniciant la contrasenya. Primària Reenvia verificació Elimina Reinicia la teva contrasenya Inscriu-te Inscriu-te Les adreces de mail següents estan associades al teu compte: No verificada Verificada Avís: Ets administrador No t'has autenticat següent anterior ja estàs autenticat com a %(user_display)s. 