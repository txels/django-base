FROM python:3.10.1

ARG MODE=""

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN adduser --gecos "" --disabled-password nonroot

# Set work directory
WORKDIR /code
RUN chown nonroot /code

# Install dependencies
# COPY Pipfile Pipfile.lock /code/
# RUN pip install pipenv && pipenv install --system $MODE
COPY pyproject.toml poetry.lock /code/
RUN pip install -U pip && pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-root

# Copy project
COPY --chown=nonroot . /code/

USER nonroot
EXPOSE 8000

# This is a costly operation, can be done outside only when static files change
# RUN python ./manage.py collectstatic --noinput

CMD [ "./entrypoint.sh" ]
