# Django-base

A base Django project with built-in authentication (web and API),
docker, kubernetes, etc.

## Customize the template

Clone this repo. Change the remote to a new repo.

Search and replace `<name>` with project name. This will be reflected in image name, and kubernetes service and deployment names.

Search and replace `<domain>` with the domain where this will be deployed to.

## Get started

Create a `.localenv` file.

Once customized, you can use the docker workflow:

    poetry lock
    just build
    just init-data

Or the off-docker option:

    # on one shell start the DB:
    . ./setvars .localenv
    just infra

    # on another shell, run the project
    . ./setvars .localenv
    poetry install --no-root
    poetry shell
    ./manage.py migrate
    ./manage.py createsuperuser --username django --email carles@barrobes.com

This will install dependencies, create a DB, initialise it by running migrations, and create a superuser named `django` with email `carles@barrobes.com`.
You will be prompted to enter a password for the superuser.

Once set up, you can run your service locally on port 8000:

    just run               # docker
    ./manage.py runserver  # metal

## Prepare for initial deployment

Review `config/settings.py` and `k8s/deployment.yml` and make sure you use new secret keys for everything. To create a fresh secret key in k8s you can run:

    just k8s-secret-key

Set up a postgres database and its secrets.

```sh
source operations/linode/.credentials
operations/tools/new-database-16 <name>
```

Create a bucket in linode with the service's name.

```sh
cd operations/linode
just bucket <name>
just website <name>
```

Finally, replace this README with one that makes sense for the target project.
